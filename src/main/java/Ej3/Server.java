package Ej3;

import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class Server {
    //establecemos el puerto y la maquina que usaremos
    private  static final int TO_PORT = 9090;
    private static final String TO_MULTICAST = "localhost";

    public static void main(String[] args) {
        //creamos una ArrayList con posibles respuestas para el cliente
        ArrayList<String> frases = new ArrayList<>();
        frases.add("Hey, como estás?");
        frases.add("Cuanto tiempo, como estás?");
        frases.add("Hombreee, como estás?");
        //creamos un random que usaremos más tarde
        Random r = new Random();
        int valorRandom = r.nextInt(2);

            //creamos el InetSocket con los datos del puerto y la maquina
            InetSocketAddress inetSocketAddress = new InetSocketAddress(TO_MULTICAST, TO_PORT);

            try (DatagramSocket s = new DatagramSocket()) {
                //asignamos un valor aleatorio a la Array de antes para que muestre un mensaje aleatorio
                String phrase = frases.get(valorRandom);
                //establece la cantidad de bytes que mostrará el mensaje
                byte[] dato = new byte[1024];
                //recibe el texto enviado por el cliente
                DatagramPacket dgp = new DatagramPacket(dato, dato.length);
                s.receive(dgp);
                String mensaje = String.valueOf(dgp);
                System.out.println(mensaje);
                //si el texto contiene la palabra 'hola' enviará al cliente uno de los valores aleatorio del array
                //y si no cerramos el datagramSocket
                if(mensaje.contains("hola")) {
                    DatagramPacket hi = new DatagramPacket(phrase.getBytes(), phrase.length(),
                            inetSocketAddress.getAddress(), inetSocketAddress.getPort());
                    s.send(hi);
                    System.out.println(phrase);
                }else{
                    s.close();
                }
            } catch (IOException e) {
                System.err.println(e.getLocalizedMessage());
            }
        }
    }

