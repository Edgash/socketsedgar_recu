package Ej2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class Cliente {

    public static void main(String[] args) {
        //establecemos el puerto y la maquina que usaremos
        String HOST = "localhost";
        int PUERTO = 9090;
        Socket sc;
        DataOutputStream salida;
        DataInputStream entrada;
        String mensajeRecibido;

        Scanner teclado = new Scanner(System.in);
        try {
            while(true) {
                //creamos el InetSocket con los datos del puerto y la maquina
                sc = new Socket(HOST, PUERTO);
                salida = new DataOutputStream(sc.getOutputStream());
                entrada = new DataInputStream(sc.getInputStream());
                String msg = "";
                //le pedimos al cliente que nos escriba algo
                System.out.println("Escriba un mensaje para enviar");
                msg = teclado.nextLine();
                //enviamos mensaje al servidor
                salida.writeUTF(msg);
                //leemos la respuesta envaida por el servidor y lo imprimimos por pantalla
                mensajeRecibido = entrada.readUTF();
                System.out.println(mensajeRecibido);
            }
        } catch (Exception ex) {
            System.out.println("");
        }
    }
}

