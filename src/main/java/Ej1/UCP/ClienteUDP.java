package Ej1.UCP;

import java.io.IOException;
import java.net.*;

public class ClienteUDP {
        //establecemos el puerto y la maquina que usaremos
        private static final int PORT_TO_LISTEN = 9090;
        private static final String FROM_MULTICAST_LISTEN = "localhost";

        public static void main(String[] args) {
            try {
                DatagramSocket ds = new DatagramSocket();
                String message = "mensaje para el servidor";
                DatagramPacket dp = new DatagramPacket(message.getBytes(), message.getBytes().length,
                        InetAddress.getByName(FROM_MULTICAST_LISTEN), PORT_TO_LISTEN);

                ds.send(dp);
                byte[] response = new byte[1024];
                dp = new DatagramPacket(response, response.length);
                ds.receive(dp);
                System.out.println(new String(response, 0, dp.getLength()));
                ds.close();
            } catch (SocketException e) {
                System.out.println("Socket: " + e.getMessage());
            } catch (IOException e) {
                System.out.println("IO: " + e.getMessage());
            }
        }
}

