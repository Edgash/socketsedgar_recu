package Ej1.UCP;

import java.io.IOException;
import java.net.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServidorUDP {

        //establecemos el puerto y la maquina que usaremos
        private static final int TO_PORT = 9090;
        private static final String TO_MULTICAST = "localhost";

        public static void main(String[] args) {

            try {
                InetSocketAddress isa = new InetSocketAddress(TO_MULTICAST, TO_PORT);
                DatagramSocket ds = new DatagramSocket(isa);

                byte[] message = new byte[1024];
                DatagramPacket dp = new DatagramPacket(message, message.length);

                ds.receive(dp);
                System.out.println(new String((message)));

                InetAddress ia = dp.getAddress();
                int senderPort = dp.getPort();

                Date fecha = new Date();
                String mensaje = "BIENVENIDO AL SERVIDOR SERENO, LA FECHA Y LA HORA ACTUALES SON: " + String.valueOf(fecha);

                byte[] messageBack = mensaje.getBytes();
                DatagramPacket dpBack = new DatagramPacket(messageBack, messageBack.length, ia, senderPort);

                ds.send(dpBack);
                ds.close();

            }catch(SocketException e){
                System.out.println(e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
}
